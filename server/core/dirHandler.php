<?
    // Define your global url and dir variables here
    $urlList = array(
        "/core",
        "/libs",
        "/assets",
        "/data"
    );

    $pathinfo = pathinfo(__DIR__);
    $urlRoot = $pathinfo['dirname'];
    $urlRoot = str_replace("\\", "/", $urlRoot); // windows fix
    $urlRoot = str_replace($_SERVER['DOCUMENT_ROOT'], "/", $urlRoot);
    $urlRoot = "/".ltrim($urlRoot, "/");
    $dirRoot = $_SERVER['DOCUMENT_ROOT'].$urlRoot;
    #var_dump($urlRoot);
    #var_dump($dirRoot);

    foreach ($urlList as $key => $value) {
        $key = str_replace("/", "", $value);
        $key = ucfirst($key);
        ${"dir$key"} = $dirRoot.$value;
        ${"url$key"} = $urlRoot.$value;
    }
?>