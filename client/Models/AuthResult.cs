﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models
{
    public class AuthResult
    {
        public string Token { get; set; }
        public string IpInternal { get; set; }
        public string IpExternal { get; set; }
        public string HostName { get; set; }
        public bool IsAuthenticated
        {
            get
            {
                // Check if the token value has the length of a standard SHA1 value (which is 40)
                if (string.IsNullOrEmpty(Token)) return false;
                return Token.Length == 40;
            }
        }

    }
}
