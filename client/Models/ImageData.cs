﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Models
{
    public class ImageData
    {
        public string Image { get; set; }
        public string Token { get; set; }
    }
}
